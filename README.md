# Smart Brace
Designed a smart brace for scoliotic patients by employing of piezo electric sensors. Also, developed a website for this product.

## Website

<img src="Website Screenshots/photo_2021-09-10_00-02-57.jpg" alt="Brochure" width="400"/>

## Poster

<img src="Poster/Poster1.jpg" alt="Brochure" width="400"/>

## Brochure

<img src="Brochure/2.jpg" alt="Brochure" width="400"/>
